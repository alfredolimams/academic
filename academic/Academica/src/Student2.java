import java.util.ArrayList;

public class Student2 extends People {

	private ArrayList<Publications> publications;
	
	public Student2(String name, String cpf, String email) {
		super(name, cpf, email);
		publications = new ArrayList<Publications>();
		// TODO Auto-generated constructor stub
	}

	public ArrayList<Publications> getPublications() {
		return publications;
	}

	public void setPublications(ArrayList<Publications> publications) {
		this.publications = publications;
	}
	public void AddPublications( Publications publication ){
		publications.add(publication);
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String to = super.toString() + "\n";
		for (int i = 0; i < publications.size() ; i++) {
			to += publications.get(i) + "\n";
		}
		return to;
	}
	
}
