import java.util.ArrayList;

public class Student extends People {

	private boolean active;
	private ArrayList<Publications> publications;
	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public ArrayList<Publications> getPublications() {
		return publications;
	}


	public void setPublications(ArrayList<Publications> publications) {
		this.publications = publications;
	}


	public Student( String name, String cpf, String email ) {
		// TODO Auto-generated constructor stub
		super(name, cpf, email);
		this.active = false;
		publications = new ArrayList<Publications>();
	}
	
	public void AddPublications( Publications publication ){
		publications.add(publication);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String to = super.toString() + "\n";
		for (int i = 0; i < publications.size() ; i++) {
			to += publications.get(i) + "\n";
		}
		return to;
	}
}
