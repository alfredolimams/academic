import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


public class Manager {
	
	private Map< String , Projects > development;
	private Map< String , People > contributors;
	private Map< String , Projects > progress;
	private Map< String , Projects > completed;
	private LinkedList<Publications> publications;
	private LinkedList<Orientation> orientations;
 	public Manager() {
		development = new HashMap< String, Projects >();
		progress = new HashMap< String, Projects >();
		completed = new HashMap< String, Projects >();
		contributors = new HashMap< String, People >();
		publications = new LinkedList<Publications>();
		orientations = new LinkedList<Orientation>();
	}
	
	public Map<String, Projects> getDevelopment() {
		return development;
	}
	public void setDevelopment(Map<String, Projects> projects) {
		this.development = projects;
	}
	public Map<String, People> getContributors() {
		return contributors;
	}
	public void setContributors(Map<String, People> contributors) {
		this.contributors = contributors;
	}
	
	public boolean addContributors( People people ){
		
		if( contributors.containsKey( people.cpf  ) ){
			System.out.println("Usuário já cadastrado!");
			return false;
		}
		contributors.put( people.cpf, people );
		return true;
	}
	
	public boolean AddProjects( Projects project ){
		
		if( development.containsKey( project.getTitle() ) ){
			System.out.println("Titulo já cadastrado!");
			return false;
		}
		development.put( project.getTitle() , project );
		return true;		
	}
	
	public Projects searchProject( String title, boolean print ){
		if( development.containsKey( title ) ){
			return development.get(title);
		}
		else if( progress.containsKey( title ) ){
				return progress.get(title);
		}
		else if( completed.containsKey( title ) ){
			return completed.get(title);
		}
		if( print ) System.out.println("Nenhum projeto com esse titulo!");
		return null;
	}
	
	public People searchContributors( String cpf, boolean print ){
		if( contributors.containsKey( cpf ) ){
			return contributors.get(cpf);
		}
		if( print ) System.out.println("Não tem esse colaborador ");
		return null;
	}	
	public Map<String, Projects> getProgress() {
		return progress;
	}

	public void setProgress(Map<String, Projects> progress) {
		this.progress = progress;
	}

	public Map<String, Projects> getCompleted() {
		return completed;
	}

	public void setCompleted(Map<String, Projects> completed) {
		this.completed = completed;
	}
	
	public Orientation addOrientation( Orientation orientation ){
		orientations.add(orientation);
		return orientation;
	}
	public Publications addPublications( Publications publication ){
		publications.add(publication);
		return publication;
	}

	public LinkedList<Publications> getPublications() {
		return publications;
	}

	public void setPublications(LinkedList<Publications> publications) {
		this.publications = publications;
	}

	public LinkedList<Orientation> getOrientations() {
		return orientations;
	}

	public void setOrientations(LinkedList<Orientation> orientations) {
		this.orientations = orientations;
	}
	public void change( String title ){
		Projects project = searchProject(title, false);
		if( project.getStatus().compareToIgnoreCase("Em elaboração") == 0 ){
			project.setStatus("Em andamento");
			development.remove(title);
			progress.put(title, project);
		}
		else if( project.getStatus().compareToIgnoreCase("Em andamento") == 0 ){
			project.setStatus("Concluido");
			progress.remove(title);
			completed.put(title, project);
		}
	}
}
