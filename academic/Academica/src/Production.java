import java.util.ArrayList;
import java.util.GregorianCalendar;

public class Production {

	protected ArrayList<People> authors;
	protected String title;
	protected String conference;
	protected GregorianCalendar year;
	protected Projects project;
	
	
	public Production( String title, String conference, GregorianCalendar year, Projects project ) {
		
		this.title = title;
		this.conference = conference;
		this.year = year;
		this.project = project;
		authors = new ArrayList<People>();
	}
	
	public ArrayList<People> getAuthors() {
		return authors;
	}


	public void setAuthors(ArrayList<People> authors) {
		this.authors = authors;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getConference() {
		return conference;
	}


	public void setConference(String conference) {
		this.conference = conference;
	}


	public GregorianCalendar getYear() {
		return year;
	}


	public void setYear(GregorianCalendar year) {
		this.year = year;
	}


	public Projects getProject() {
		return project;
	}


	public void setProject(Projects projects) {
		this.project = projects;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.title;
	}
}
