import java.util.GregorianCalendar;
import java.util.LinkedList;


public class Projects {

	private String title;
	private GregorianCalendar start;
	private GregorianCalendar end;
	private String fuding;
	private String business;
	private String description;
	private LinkedList<People> contributors;
	private LinkedList<Production> productions;
	private String status;
	private Float value;
	public LinkedList<Production> getProductions() {
		return productions;
	}

	public void setProductions(LinkedList<Production> productions) {
		this.productions = productions;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Projects( String title ) {
		// TODO Auto-generated constructor stub
		this.title = title;
		this.start = null;
		this.end = null;
		this.fuding = null;
		this.business = null;
		this.description = null;
		this.value = null;
		contributors = new LinkedList<People>();
		this.status = "Em elaboração";
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public GregorianCalendar getStart() {
		return start;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public void setStart(GregorianCalendar start) {
		this.start = start;
	}

	public GregorianCalendar getEnd() {
		return end;
	}

	public void setEnd(GregorianCalendar end) {
		this.end = end;
	}

	public String getFuding() {
		return fuding;
	}

	public void setFuding(String fuding) {
		this.fuding = fuding;
	}

	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LinkedList<People> getContributors() {
		return contributors;
	}

	public void setContributors(LinkedList<People> contributors) {
		this.contributors = contributors;
	}
	public void addContributors( People people ){
		contributors.add(people);
	}
	public void addProduction( Production production ){
		productions.add(production);
	}
	
	@Override
	public String toString() {
		String to = this.title + " " + this.fuding + "\n";
		to += ((this.value) + " ") + this.status + "\n";
		to += this.business + " " + this.description + "\n";
		to += "Contribuintes: \n";
		for (int i = 0; i < contributors.size() ; i++) {
			to += contributors.get(i).name + "\n";
		}
		to += "Produções: \n";
		for (int i = 0; i < contributors.size() ; i++) {
			to += productions.get(i).title + "\n";
		}		
		return to;
	}
	
	public boolean haveTeacher(){
		
		for (int i = 0; i < contributors.size() ; i++) {
			if (contributors.get(i) instanceof Teacher) {
				return true;			
			}
		}
		return false;
	}
	
}
