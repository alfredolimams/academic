import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


public class Commands {

	private Scanner input;
	private Manager manager;
	public Commands( Scanner input ){
		this.input = input;
		this.manager = new Manager();
	}
	
	public void menu(){
		System.out.println("Menu:");
		System.out.println("1 - Criar Projeto, 2 - Cadastrar Colaborador");
		System.out.println("3 - Alocar Colaborador, 4 - Iniciar/Finalizar Projeto");
		System.out.println("5 - Produção Academica, 6 - Consultar Colaborador");
		System.out.println("7 - Consultar Projeto, 8 - Lista de Produção");
		System.out.println("9 - Lista de Colaboradores , 10 - Lista de Projetos em Elaboração");
		System.out.println("11 - Lista de Projetos em andamento , 12 - Lista de Projetos Concluidos");
		System.out.println("13 - Lista de Projetos ");
		
	}
	
	public void read(){
		String command = input.nextLine();
		System.out.println( "Comando foi " + command + " " + command.length() );
		if( command.compareToIgnoreCase("1") == 0 ){
			registryProject();
		}
		else if( command.compareToIgnoreCase("2") == 0 ){
			registryPeople();
		}
		else if( command.compareToIgnoreCase("3") == 0 ){
			allocate();
		}
		else if( command.compareToIgnoreCase("4") == 0 ){
			statusProjects();
		}
		else if( command.compareToIgnoreCase("5") == 0 ){
			production();
		}
		else if( command.compareToIgnoreCase("6") == 0 ){
			consultPeople();
		}
		else if( command.compareToIgnoreCase("7") == 0 ){
			consultProject();
		}
		else if( command.compareToIgnoreCase("8") == 0 ){
			listProduction();
		}
		else if( command.compareToIgnoreCase("9") == 0 ){
			listPeople();
		}		
		else if( command.compareToIgnoreCase("10") == 0 ){
			listProgress();
		}
		else if( command.compareToIgnoreCase("11") == 0 ){
			listDevelopment();
		}
		else if( command.compareToIgnoreCase("12") == 0 ){
			listCompleted();
		}
		else if( command.compareToIgnoreCase("13") == 0 ){
			listAll();
		}
		else {
			System.out.println("Comando Inválido");
		}
	}
	
	private void registryPeople(){
		
		String cpf = getCpf();
		System.out.println(cpf);
		if( !validCpf(cpf) ){
			return;
		}
		
		String name = getName();
		System.out.println(name);
		if( !validNames(name) ){
			return;
		}
		
		String email = getEmail();
		
		while( true ){
			System.out.println("Colaborador: 1 - Professor ou Pesquisador , 2 - Graduando, 3 - Mestrando ou Doutorando");
			String comand = input.nextLine();
			if( comand.compareToIgnoreCase("1") == 0 ){
				manager.addContributors( new Student(name, cpf, email) );
				return;
			}
			else if( comand.compareToIgnoreCase("2") == 0 ){
				manager.addContributors( new Student2(name, cpf, email) );
				return;				
			}
			else if( comand.compareToIgnoreCase("3") == 0 ){
				manager.addContributors( new Teacher(name, cpf, email) );
				return;
			}
			else {
				System.out.println("Comando errado");
			}
		}
	}
	private void registryProject(){
		String title = getTitle();
		Projects projects;
		if( manager.AddProjects( new Projects(title) ) ){
			projects = manager.searchProject(title, false);
		}
		else {
			return;
		}
		projects.setDescription( getDescription() );
		projects.setFuding( getFound() );
		projects.setBusiness( getBusiness() );
		projects.setValue( getValue() );
	}
	private void consultPeople(){
		String cpf = getCpf();
		People people = manager.searchContributors(cpf, true);
		if( people != null ){
			if (people instanceof Student) {
				Student student = (Student) people;
				System.out.println(student);
			}
			else if (people instanceof Student2 ) {
				Student2 student2 = (Student2) people;
				System.out.println(student2);
			}
			else {
				Teacher teacher = (Teacher) people;
				System.out.println(teacher);
			}
		}
	}
	private boolean validCpf( String cpf ){
		
		if( cpf.isEmpty() ){
			System.out.println("Campo do CPF Vazio");
			return false;
		}
		else if( manager.searchContributors(cpf,false) != null ){
			System.out.println("CPF Cadastrado");
			return false;			
		}
		else if( !validNumbers(cpf) ){			
			System.out.println("Inválido");
			return false;
		}
		return true;
	}
	private boolean validNumbers( String number ){
		for (int i = 0; i < number.length() ; ++i ) {
			if( number.charAt(i) < '0' || number.charAt(i) > '9' ){
				return false;
			}
		}
		return true;
	}
	private boolean validNames( String name ){
		for (int i = 0; i < name.length() ; ++i ) {
			if( name.charAt(i) < 'A' || name.charAt(i) > 'z' || name.charAt(i) != ' ' ){
				System.out.println("Inválido");
				return false;
			}
		}
		return true;
	}

	private String getCpf(){
		System.out.print("Digite o CPF: ");
		return input.nextLine();		
	}
	private String getName(){
		System.out.print("Digite o Nome: ");
		return input.nextLine();		
	}
	private String getEmail(){
		System.out.print("Digite o Email: ");
		return input.nextLine();
	}
	private String getTitle(){
		System.out.println("Digite o titulo");
		return input.nextLine();
	}
	private String getDescription(){
		System.out.println("Informe a descrição do Projeto");
		return input.nextLine();
	}
	private String getFound(){
		System.out.println("Fundo do Projeto");
		return input.nextLine();
	}
	private String getBusiness(){
		System.out.println("Objetivos do Projeto");
		return input.nextLine();
	}
	private float getValue(){
		System.out.println("Digite o valor financiado");
		return input.nextFloat();
	}

	private void listPeople(){
		System.out.println("Lista de Colaboradores:");
		System.out.println("Tem: " + manager.getContributors().size() + " Colaboradores");
		Map< String , People > contributors = manager.getContributors();
		Set<String> keys = contributors.keySet();
		for ( String key : keys ) {
			System.out.println( contributors.get(key) );
		}
	}
	private void listCompleted(){
		System.out.println("Lista de Projetos Completados:");
		System.out.println("Tem: " + manager.getCompleted().size() + " Projetos");
		Map< String , Projects > projects = manager.getCompleted();
		Set<String> keys = projects.keySet();
		for ( String key : keys ) {
			System.out.println( projects.get(key) );
		}
	}
	private void listDevelopment(){
		System.out.println("Lista de Projetos Completados:");
		System.out.println("Tem: " + manager.getDevelopment().size() + " Projetos");
		Map< String , Projects > projects = manager.getDevelopment();
		Set<String> keys = projects.keySet();
		for ( String key : keys ) {
			System.out.println( projects.get(key) );
		}
	}	
	private void listProgress(){
		System.out.println("Lista de Projetos Completados:");
		System.out.println("Tem: " + manager.getProgress().size() + " Projetos");
		Map< String , Projects > projects = manager.getProgress();
		Set<String> keys = projects.keySet();
		for ( String key : keys ) {
			System.out.println( projects.get(key) );
		}
	}
	private void listAll(){
		int all = manager.getCompleted().size();
		all += manager.getDevelopment().size();
		all += manager.getProgress().size();
		System.out.println("Total de projetos: " + all );
		listProgress();
		listDevelopment();
		listCompleted();
	}
	private void listProduction(){
		System.out.println("Tem " + ( manager.getOrientations().size() + manager.getPublications().size() ) + " produções" );
		System.out.println("Sendo " + manager.getOrientations().size() + "Orientações" );
		LinkedList list = manager.getOrientations();
		for (int i = 0; i < list.size() ; i++) {
			Orientation orientation = (Orientation) list.get(i);
			System.out.println( orientation );
		}
		System.out.println("Sendo " + manager.getPublications().size() + "Publicações" );
		list = manager.getPublications();
		for (int i = 0; i < list.size() ; i++) {
			Publications publication = (Publications) list.get(i);
			System.out.println( publication );
		}
	}
	private String command(){
		System.out.print("Digite o comando: ");
		return input.nextLine();
	}
	private void production(){
		String cpf = getCpf();
		People people;
		if( validCpf(cpf) ){
			people = manager.searchContributors(cpf, true);
			if( people == null ){
				return;
			}
		}
		else{
			return;
		}
		System.out.print("Projeto Associado ");
		String titleProject = getTitle();
		Projects project = manager.searchProject(titleProject, false);
		if( project == null ){
			System.out.println("Nenhum projeto com esse titulo");
		}
		else{
			if( project.getStatus() == "Em andamento" ){
				
			} else{
				project = null;
				System.out.println("Projeto não tá em andamento");
			}
		}
		String title = getTitle();
		System.out.println("Digite a conferencia");
		String conference = input.nextLine();
		
		System.out.println("Tipo de produção academica");
		System.out.println("1 - Publicações, 2 - Orientações");
		String c = command();
		Production production = null;
		if( c.compareToIgnoreCase("1") == 0  ){
			Publications publication = new Publications(title, conference, new GregorianCalendar() , project);
			production = manager.addPublications( publication );
			
		}
		else if( c.compareToIgnoreCase("2") == 0 ){
			
			if ( people instanceof Teacher ) {
				
				production = manager.addOrientation( new Orientation(title, conference, new GregorianCalendar() , project) );
				
			}
			else{
				System.out.println("Colaborador não tem essa licença");
			}
			
		}
		else{
			return;
		}
		project.addProduction(production);
	}

	private void allocate(){
		String cpf = getCpf();
		People people = manager.searchContributors(cpf, true);
		if (people instanceof Student) {
			Student student = (Student) people;
			if( student.isActive() ){
				System.out.println("Usuario já alocado");
				return;
			}
			else {
				student.setActive(true);
			}
		}
		listProgress();
		System.out.println("Escolha um Projeto");
		String title = getTitle();
		Projects project = manager.searchProject(title, true);
		if( project == null ){
			return;
		}
		else{
			project.addContributors(people);
		}
	}
	private void consultProject(){
		String title = getTitle();
		Projects project = manager.searchProject(title, true);
		System.out.println(project);
	}
	private void deallocate( LinkedList<People> peoples ){
		for (int i = 0; i < peoples.size() ; i++) {
			if (peoples.get(i) instanceof Student) {
				Student student = (Student) peoples.get(i);
				student.setActive(false);
			}
		}
	}
	private void statusProjects(){
		String title = getTitle();
		Projects project = manager.searchProject(title, true);
		if( project == null ){
			return;
		}
		if( project.getStatus().compareToIgnoreCase("Em andamento") == 0  ){
			
			if( project.getProductions().size() > 0 ){
				deallocate( project.getContributors() );
				project.setEnd( new GregorianCalendar() );
				manager.change(title);
			}
			
		}
		else if( project.getStatus().compareToIgnoreCase("Em elaboração") == 0 ){
			
			if( project.getContributors().size() > 0 && project.haveTeacher() ){
				project.setStart( new GregorianCalendar() );
				manager.change(title);
			}
			
		}
	}
	
}
